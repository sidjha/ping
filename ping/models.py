from ping import db

class User(db.Model):
	# user attributes
	id = db.Column(db.Integer, primary_key=True)
	firstName = db.Column(db.String(35))
	lastName = db.Column(db.String(40))
	email = db.Column(db.String(75), unique=True)
	password = db.Column(db.String(40))

	def __init__(self, firstName, lastName, email, password):
		self.firstName = firstName
		self.lastName = lastName
		self.email = email
		self.password = password

	def __repr__(self):
		return '<User firstName: %s, lastName: %s, email: %s, password: %s>' % (self.firstName, self.lastName, self.email, self.password)

class Business(db.Model):
	# business attributes
	id = db.Column(db.Integer, primary_key=True)
	businessName = db.Column(db.String(80))
	address = db.Column(db.String(80))
	city = db.Column(db.String(40))
	postal = db.Column(db.String(6))
	state = db.Column(db.String(20))
	country = db.Column(db.String(20))
	phone = db.Column(db.Integer)
	email = db.Column(db.String(35))

	def __init__(self,
				businessName,
				address,
				city,
				postal,
				state,
				country,
				phone, 
				email
				):

		self.businessName = businessName
		self.address = address
		self.city = city
		self.postal = postal
		self.state = state
		self.country = country
		self.phone = phone
		self.email = email

	def __repr__(self):
		return '<Biz %s>' % (self.businessName)

class Manager(db.Model):
	# manager attributes
	id = db.Column(db.Integer, primary_key=True)
	businessId = db.Column(db.Integer, db.ForeignKey('business.id'))
	managerFirstName = db.Column(db.String(35))
	managerLastName = db.Column(db.String(40))
	managerEmail = db.Column(db.String(75))
	password = db.Column(db.String(40))

	def __init__(self,
				businessId,
				managerFirstName,
				managerLastName,
				managerEmail,
				password
				):
		self.businessId = businessId
		self.managerFirstName = managerFirstName
		self.managerLastName = managerLastName
		self.managerEmail = managerEmail
		self.password = password
	
	def __repr__(self):
		return '<Manager %s>' % (self.managerEmail)