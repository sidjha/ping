from wtforms import Form, TextField, PasswordField, validators

class BusinessRegistrationForm(Form):
	businessName = TextField('Business Name', [validators.Required()])
	address = TextField('Address', [validators.Required()])
	city = TextField('City', [validators.Required()])
	postal = TextField('Zip', [validators.Required(), validators.Length(min=5, max=6)])
	state = TextField('State', [validators.Required()])
	country = TextField('Country', [validators.Required()])
	phone = TextField('Phone', [validators.Required()])

class ManagerRegistrationForm(Form):
	managerFirstName = TextField('First Name', [validators.Required()])
	managerLastName = TextField('Last Name', [validators.Required()])
	managerEmail = TextField('Email', [validators.Email()])
	password = PasswordField('Password', [validators.Required()])

class UserRegistrationForm(Form):
	firstName = TextField('First Name', [validators.Required()])
	lastName = TextField('Last Name', [validators.Required()])
	email = TextField('Email', [validators.Email()])
	password = PasswordField('Password', [validators.Required()])

class LoginForm(Form):
	email = TextField('Email', [validators.Required(), validators.Email()])
	password = PasswordField('Password', [validators.Required()])