/* initialize node */
var html = require('fs').readFileSync(__dirname+'/templates/dashboard.html');
var server = require('http').createServer(function(req, res) {
    res.end(html);
});
server.listen(8080);

/* initialize nowjs */
var nowjs = require("now");
var everyone = nowjs.initialize(server);

/* connect to redis */
var redis = require("redis"), client = redis.createClient();
client.on("error", function (err) {
    console.log("Error " + err);
});

/* global constants for databases */   
var DB_CLIENTS = 0;     // db 0: keeps track of all online clients 
var DB_MESSAGES = 1;    // db 1: stores all messages

nowjs.on('connect', function () {
    console.log("Client " + this.user.clientId + " connected");
    connectClient(this.now, this.user.clientId);
});

nowjs.on('disconnect', function () {
    console.log("Client " + this.user.clientId + " disconnected");
    disconnectClient(this.now, this.user.clientId);
}); 

/* client connect and disconnect */
function connectClient (now, clientId) {
    console.log("connected: " + now.id);

    client.select(DB_CLIENTS);
    client.hmset("clients:" + now.id, "clientId", clientId, "connect_timestamp", new Date().getTime());

    client.hgetall("clients:" + now.id, function (err, obj) {
        console.dir(obj);
    });
};

function disconnectClient (now, clientId) {
    console.log("disconnected: " + now.id);

    client.select(DB_CLIENTS);
    client.del("clients:" + now.id);

    client.hgetall("clients:" + now.id, function (err, obj) {
        console.dir(obj);
    });
};

/* message distribution */
everyone.now.sendMessage = function(message, recipientId) {
    senderId = this.now.id;
    senderName = this.now.name;

    // save message in messages db
    client.select(DB_MESSAGES);
    client.incr("counter");
    client.get("counter", function (err, obj) {
        client.hmset("messages:" + obj, "id", obj, "message", message, "senderId", senderId, "recipientId", recipientId, "timestamp", new Date().getTime());    
    });   

    // get clients
    client.select(DB_CLIENTS);
    client.hgetall("clients:" + recipientId, function (err, obj) {
        if (obj.hasOwnProperty("clientId")) {
            nowjs.getClient(obj.clientId, function() {
                this.now.receiveMessage(senderId, senderName, message);
            });
        } else {
            console.log("client is not online...");
        }
    });
}