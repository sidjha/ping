"""A Yelp API v2 library"""

import oauth2
import urllib2
import urllib
import json

def lookupBusiness(biz_name, biz_location, lat, lng):
	# set up the actual request parameters
	url_params = {}
	url_params['term'] = biz_name
	if lat and lng:
		url_params['ll'] = "%s,%s" % (lat, lng)
	else:
		url_params['location'] = biz_location
	
	# number of matching business to be returned
	url_params['limit'] = 5
	
	# URL encode parameters
	encoded_params = ''
	encoded_params = urllib.urlencode(url_params)
	url = 'http://api.yelp.com/v2/search?%s' % encoded_params
	
	response = makeRequest(url)
	return json.dumps(response, sort_keys=True, indent=2)

def makeRequest(url):
	signed_url = generateSignedURL(url)

	try:
		conn = urllib2.urlopen(signed_url, None)
		try:
			response = json.loads(conn.read())
		finally:
			conn.close()
	except urllib2.HTTPError, error:
		response = json.loads(error.read())
	return response

def generateSignedURL(url):
	# add oauth parameters to the request URL

	consumer_key = 'nR87RpKZdyp7uyC3fFUR7Q' #'0P647pEQpb48bIJxpDfkXg'
	consumer_secret = 'B9uC_846jqMoLL1dip4ezWunRb4' #'61OK_8d2YRkCFiAKnjZIa6-DpKE'
	token = 'QvPpm-qAVhNicUDAiYYNWkUZ-xw7FYcK' #'g3j3mF9zqBndHvJGjRG-Hv6ZDkQ3wnJI'
	token_secret = 'IrK4lMYu3L6BKnIPwGSjeD3H1V0' #'Mrq2VMY9-IpHI28uqfhvxSWC-G0'

	consumer = oauth2.Consumer(consumer_key, consumer_secret)

	oauth_request = oauth2.Request('GET', url, {})
	oauth_request.update({'oauth_nonce': oauth2.generate_nonce(),
	                      'oauth_timestamp': oauth2.generate_timestamp(),
	                      'oauth_token': token,
	                      'oauth_consumer_key': consumer_key})

	token = oauth2.Token(token, token_secret)

	oauth_request.sign_request(oauth2.SignatureMethod_HMAC_SHA1(), consumer, token)

	signed_url = oauth_request.to_url()
	
	return signed_url
