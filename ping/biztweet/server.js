var fs    = require('fs'),
    path  = require('path'),
    http  = require('http'),
    https = require('https'),
    faye  = require('faye');

// faye.Logging.logLevel = 'debug';

var PUBLIC_DIR = path.dirname(__filename) + '/public',
    
    bayeux     = new faye.NodeAdapter({
		  mount: '/bayeux', 
		  timeout: 20,
		  engine: {
		    type:	'redis',
		    host: 'localhost',
		    port: '6379',
		    //password:   'REDIS_AUTH',
		    database:   0,
		    namespace: '/foo'
		  }
		}),
    port       = process.argv[2] || '8000',
    secure     = process.argv[3] === 'ssl';

var handleRequest = function(request, response) {
  var path = (request.url === '/') ? '/index.html' : request.url;
  
  fs.readFile(PUBLIC_DIR + path, function(err, content) {
    var status = err ? 404 : 200;
    response.writeHead(status, {'Content-Type': 'text/html'});
    response.write(content || 'Not found');
    response.end();
  });
};

var server = secure
           ? https.createServer(handleRequest)
           : http.createServer(handleRequest);

bayeux.attach(server);
server.listen(Number(port));

bayeux.getClient().subscribe('/chat/*', function(message) {
  console.log('[' + message.user + ']: ' + message.message);
});

console.log('Listening on ' + port + (secure? ' (https)' : ''));

