BizTweet = {
  /**
   * Initializes the application, passing in the globally shared Bayeux
   * client. Apps on the same page should share a Bayeux client so
   * that they may share an open HTTP connection with the server.
   */
  init: function(bayeux) {
    var self = this;
    this._bayeux = bayeux;
    
    //var redis = require('redis');
    //this._redis = redis.createClient();
    
    this._login   = $('#enterCredentials');
    this._registerUser = $('#registerUser');
    this._app     = $('#app');
    this._follow  = $('#addFollowee');
    this._post    = $('#postMessage');
    this._stream  = $('#stream');
     
    // TODO: hook this up to the Business table
    this._companies = ["bestbuy", "starbucks", "futureshop"]; 
    
    this._app.hide();
    
    // When the user enters a username, store it and start the app
    this._login.submit(function() {

      // TODO: get the handle directly from the current session
      var handle = $('#handle').val();

      //checks whether handle is a registered company
      if (self._companies.indexOf(handle) >= 0) {
	       self._handletype = "biz";
      } else { 
	       self._handletype = "user";
      }
      self._handle = handle; 
      self.launch();
      return false;
    });
  },

  /**
   * Starts the application after a username has been entered. A
   * subscription is made to receive messages that mention this user,
   * and forms are set up to accept new followers and send messages.
   */
  launch: function() {
    var self = this;
    if (this._handletype == "biz") {
       self._bayeux.subscribe('/user/tobiz/' + self._handle, self.accept, self);
    }
    else {
         //self._bayeux.subscribe('/biz/' + biz + '/users/' + self._handle, self.accept, self);
    } 
    
    this._registerUser.fadeOut('slow');
    // Hide login form, show main application UI
    this._login.fadeOut('slow', function() {
      self._app.fadeIn('slow');
    });
    
    // When we add a follower, subscribe to a channel to which the
    // followed user will publish messages
    this._follow.submit(function() {
      var follow = $('#followee'),
          name   = follow.val();
      
      self._bayeux.subscribe('/biz/' + name + '/users/' + self._handle, self.accept, self);

      //self._bayeux.subscribe('/biz/' + name, self.accept, self);
      follow.val('');
      return false;
    });
    
    // When we enter a message, send it and clear the message field.
    this._post.submit(function() {
      var msg = $('#message');
      self.post(msg.val());
      msg.val('');
      return false;
    });
  },
  
  /**
   * Sends messages that the user has entered. The message is scanned for
   * @reply-style mentions of other users, and the message is sent to those
   * users' channels.
   */
  post: function(message) {
    var mentions = [],
        words    = message.split(/\s+/),
        self     = this,
        pattern  = /\@[a-z0-9]+/i;
    
    // Extract @replies from the message
    $.each(words, function(i, word) {
      if (!pattern.test(word)) return;
      word = word.replace(/[^a-z0-9]/ig, '');
      if (word !== self._handle) mentions.push(word);
    });
    
    // Message object to transmit over Bayeux channels
    message = {user: this._handle, message: message};
    
    // Publish to this user's 'from' channel, and to channels for any
    // @replies found in the message
    // TODO: if Biz, publish only to the subscribers of this Biz 
    //       if User, publish to any Biz 
    if (this._handletype == "biz") {
      //self._bayeux.publish('/biz/' + self._handle, message);
      
      //publish each mention to channel of biz's subscriber 
      $.each(mentions, function(i, name) {
        self._bayeux.publish('/biz/' + self._handle + '/users/' + name, message);
      });

    } else {
      //self._bayeux.publish('/user/' + self._handle, message);
      
      $.each(mentions, function(i, name) {
        self._bayeux.publish('/user/tobiz/' + name, message);
      });

    } 
  },
  
  /**
   * Handler for messages received over subscribed channels. Takes the
   * message object sent by the post() method and displays it in
   * the user's message list.
   */
  accept: function(message) {
    this._stream.prepend('<li><b>' + message.user + ':</b> ' +
                                     message.message + '</li>');
  }
};
