var lastJSONResponse = null;

  
  function showLocation(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    console.log(latitude);
    console.log(longitude);
  }

  function locationError(err) {
    console.log(err);
  }
  
  // Get current location for the HTML5 GeoLocation API
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showLocation, locationError);
  }

  function searchBiz(term) {
    $.getJSON('/_search_yelp', 
          {name: term, lat: latitude, lng: longitude},

          function(data) {
              // if any businesses
              if (data.businesses.length > 0) {
                lastJSONResponse = data; // store this JSON response globally so it can be read later
                $.each(data.businesses, function(i, biz) {
                    updateResults(biz.name, biz.location.address);
                });

                // listens when the user selects a business from the results list
                $("div.item").click( function() {
                  // which item did the user click
                  var index = $("div.item").index(this);
                  bizClicked(index);
                });
              }
              // if no businesses
              else {
                noResults();
              }

          });
  }

  // output a list of matching businesses in the UI
  function updateResults(name, address) {
    $('#results').append('<div class="item">' + '<p class="name">' + name + '</p>' + '<p class="address">' + address + '</p></div>');
  }

  // output a not found message
  function noResults() {
    $('#results').append('No businesses found');
  }