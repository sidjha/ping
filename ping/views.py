from ping import app, db
from flask import request, render_template, redirect, flash, url_for, jsonify, g, session, escape

from models import User, Business, Manager
from forms import BusinessRegistrationForm, ManagerRegistrationForm, UserRegistrationForm, LoginForm

from werkzeug.contrib.cache import SimpleCache
cache = SimpleCache()

@app.route('/')
def index():
    return render_template("index.html")

@app.route("/register/business", methods=['POST', 'GET'])
def register_business():
    form = BusinessRegistrationForm(request.form)

    if request.method == 'POST' and form.validate():
    	d = {}
    	d['businessName'] = form.businessName.data
    	d['address'] = form.address.data
    	d['city'] = form.city.data
    	d['postal'] = form.postal.data
    	d['state'] = form.state.data
    	d['country'] = form.country.data
    	d['phone'] = form.phone.data

    	cache.set('businessinfo', d)

        return redirect(url_for("register_manager"))
    return render_template("register_business.html", form=form)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

@app.route("/register/business/manager", methods=['POST', 'GET'])
def register_manager():
	# disable access to step 2 unless step 1 is complete
	if not cache.get('businessinfo'):
		return redirect(url_for("register_business"))

	form = ManagerRegistrationForm(request.form)

	if request.method == 'POST' and form.validate():
		d = cache.get('businessinfo')
		cache.clear()

		biz = Business(d['businessName'],
					   d['address'],
					   d['city'],
					   d['postal'],
					   d['state'],
					   d['country'],
					   d['phone'], 
					   form.managerEmail.data)

		# insert and commit business first so we get an ID
		db.session.add(biz)
		db.session.commit()

		manager = Manager(biz.id,
						  form.managerFirstName.data,
						  form.managerLastName.data,
						  form.managerEmail.data,
						  form.password.data)

		db.session.add(manager)
		db.session.commit()

		flash('Thanks')
		return redirect(url_for("dashboard"))
	return render_template("register_manager.html", form=form)

@app.route("/register/user", methods=['POST', 'GET'])
def register_user():
    form = UserRegistrationForm(request.form)

    if request.method == 'POST' and form.validate():
        user = User(form.firstName.data, 
                    form.lastName.data, 
                    form.email.data,
                    form.password.data)

        db.session.add(user)
        db.session.commit()

        flash('Thanks - you are now a registered user')
        return redirect(url_for("index"))
    return render_template("register_user.html", form=form)

@app.route("/_search_yelp")
def search_yelp():
	name = request.args.get('name', '', type=str)
	loc = request.args.get('location', 'waterloo', type=str)
	lat = request.args.get('lat', '', type=str)
	lng = request.args.get('lng', '', type=str)

	import yelp
	return yelp.lookupBusiness(name, loc, lat, lng)

@app.route("/business/dashboard")
def dashboard():
	if 'user' in session:
		mgr_email = session['user']
		mgr = Manager.query.filter_by(managerEmail=mgr_email).first()
		biz = Business.query.get(mgr.businessId)
		return render_template("dashboard.html", business=biz, manager=mgr)
	return redirect(url_for('login_biz'))

@app.route("/home")
def user_home():
    if 'user' in session:
        user = User.query.filter_by(email=session['user']).first()
        businesses = Business.query.all()
        
        return render_template("user_home.html", user=user, businesses=businesses)
    return redirect(url_for('login_user'))

@app.route("/login/business", methods=['POST', 'GET'])
def login_biz():
	# send to dashboard if a user is already logged in
	if 'user' in session:
		return redirect(url_for("dashboard"))

	form = LoginForm(request.form)
	error = None

	if request.method == 'POST' and form.validate():
		if log_business_in(form.email.data, form.password.data):
			return redirect(url_for("dashboard"))
		else:
			error = "Invalid username/password"
	return render_template("login.html", form=form, error=error)

@app.route("/login/user", methods=['POST', 'GET'])
def login_user():
	# send to index if a user is already logged in
	if 'user' in session:
		return redirect(url_for("user_home"))
	
	form = LoginForm(request.form)
	error = None
	if request.method == 'POST' and form.validate():
		if log_user_in(form.email.data, form.password.data):
			return redirect(url_for("user_home"))
		else:
			error = "Invalid username/password"
	return render_template("login.html", form=form, error=error)

def log_business_in(email, password):
	# check if database has a matching email/password pair
	mgr = Manager.query.filter_by(managerEmail=email, password=password).first()
	
	# add user to session if match found, else return None
	if mgr is not None:
		session['user'] = email
        session['managerFirstName'] = mgr.managerFirstName
        session['managerLastName'] = mgr.managerLastName
        session['managerEmail'] = mgr.managerEmail

        return True
	return

def log_user_in(email, password):
    # check if database has a matching email/password pair
    usr = User.query.filter_by(email=email, password=password).first()

    # add user to session if match found, else return None
    if usr is not None:
        session['user'] = email
        session['firstName'] = usr.firstName
        session['lastName'] = usr.lastName
        session['email'] = usr.email

        return True
    return

@app.route("/logout")
def logout():
	session.pop('user', None)
	return redirect(url_for('index'))

@app.route("/mockup")
def mockup():
	return render_template("mockup.html")

@app.route("/mob")
def mob():
	if 'user' in session:
		mgr_email = session['user']
		mgr = Manager.query.filter_by(managerEmail=mgr_email).first()
		biz = Business.query.get(mgr.businessId)
		return render_template("mobile.html", business=biz, manager=mgr)
	return redirect(url_for('login_biz'))
